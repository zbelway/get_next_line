/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find42.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/25 19:29:13 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/25 21:43:28 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_find42(char *s)
{
	int		i;

	i = 0;
	while (s[i])
	{
		if (s[i] == '4' && s[i + 1] == '2')
		{
			s += i;
			return (s);
		}
		i++;
	}
	return (NULL);
}
