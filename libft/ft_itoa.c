/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/15 15:52:22 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/23 12:38:03 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <limits.h>

static int	num_len(int n)
{
	int		i;

	i = 0;
	if (n < 0)
	{
		n *= -1;
		i++;
	}
	while (n > 9)
	{
		n /= 10;
		i++;
	}
	i++;
	return (i);
}

char		*ft_itoa(int n)
{
	char	*s;
	int		len;
	int		sign;

	len = num_len(n);
	if (n == INT_MIN)
		return (ft_strdup("-2147483648"));
	s = (char *)malloc(sizeof(char) * len + 1);
	if (!s)
		return (NULL);
	s[0] = '0';
	s[len--] = '\0';
	if (n < 0)
		s[0] = '-';
	sign = (n < 0) ? -1 : 1;
	n *= sign;
	while (n)
	{
		s[len] = (((n % 10)) + '0');
		len--;
		n /= 10;
	}
	return (s);
}
