/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strnstr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 15:52:36 by zbelway           #+#    #+#             */
/*   Updated: 2015/12/03 17:50:37 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strnstr(const char *big, const char *little, size_t n)
{
	size_t	i;
	size_t	k;
	char	*str;
	char	*s;

	str = (char *)big;
	s = (char *)little;
	i = 0;
	if (!s[i])
		return (str);
	while (str[i] && i < n)
	{
		k = 0;
		while (s[k] && s[k] == str[i + k] && (i + k) < n)
		{
			k++;
			if (!s[k])
				return (str + i);
		}
		i++;
	}
	return (0);
}
