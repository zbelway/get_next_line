/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 17:44:56 by zbelway           #+#    #+#             */
/*   Updated: 2015/12/03 18:02:58 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strrchr(const char *big, int c)
{
	int		i;
	char	*str;

	i = 0;
	str = (char *)big;
	while (str[i])
		i++;
	while (i >= 0)
	{
		if (c == str[i])
			return (str + i);
		i--;
	}
	return (0);
}
