/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_onespace.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/25 20:13:48 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/25 21:41:29 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_onespace(char *s)
{
	int		i;
	char	*new;
	char	*tmp;

	new = ft_strnew(ft_strlen(s));
	i = 0;
	tmp = new;
	while (s[i])
	{
		if (ft_isspace(s[i]) == 0)
			*new++ = s[i++];
		else
		{
			*new++ = ' ';
			while (ft_isspace(s[i]) == 1)
				i++;
		}
	}
	*new = '\0';
	return (tmp);
}
