/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 17:34:17 by zbelway           #+#    #+#             */
/*   Updated: 2015/12/03 18:02:29 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strstr(const char *big, const char *little)
{
	int		i;
	int		k;
	char	*str;
	char	*s;

	str = (char *)big;
	s = (char *)little;
	i = 0;
	if (!s[i])
		return (str);
	while (str[i])
	{
		k = 0;
		while (s[k] && s[k] == str[i + k])
		{
			k++;
			if (!s[k])
				return (str + i);
		}
		i++;
	}
	return (0);
}
