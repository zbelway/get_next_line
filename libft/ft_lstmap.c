/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/15 18:15:16 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/21 23:25:51 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *lst2;
	t_list *new;

	if (!lst || !f)
		return (NULL);
	lst2 = f(lst);
	if (!lst2)
		return (NULL);
	new = lst2;
	while (lst->next)
	{
		lst = lst->next;
		if ((new->next = f(lst)) == NULL)
			return (lst2);
		new = new->next;
	}
	return (lst2);
}
